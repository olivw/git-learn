package com.example.demo.learn.web;

import com.example.demo.learn.java.IPersons;
import com.example.demo.learn.java.Student;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2021/4/1
 */
@RestController
@RequestMapping("/test")
public class TestController {

    @RequestMapping("/string")
    public String hello() {
        return "hello";
    }

    @RequestMapping("/stringlist")
    public List<String> stringList() {
        List<String> list = new ArrayList<>();
        list.add("xiaoming");
        list.add("xiaohong");
        return list;
    }

    @RequestMapping("/student")
    public Student student() {
        Student xiaoming = new Student();
        xiaoming.setName("xiaoming");
        xiaoming.setAge(20);
        xiaoming.setScore(100);
        return xiaoming;
    }

    @RequestMapping("/students")
    public List<Student> students() {
        List<Student> list = new ArrayList<>();

        Student xiaoming = new Student();
        xiaoming.setName("xiaoming");
        list.add(xiaoming);

        Student xiaohong = new Student();
        xiaohong.setName("xiaohong");
        list.add(xiaohong);
        return list;
    }

    @RequestMapping("/studentmap")
    public Map<String, Student> studentMap(){

        Map<String, Student> map = new HashMap<>();

        Student xiaoming = new Student();
        xiaoming.setName("xiaoming");

        Student xiaohong = new Student();
        xiaohong.setName("xiaohong");

        map.put("1", xiaohong);
        map.put("2", xiaoming);

        return map;
    }

}
