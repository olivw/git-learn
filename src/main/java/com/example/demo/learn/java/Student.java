package com.example.demo.learn.java;

/**
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2021/3/31
 */
public class Student implements IPersons {
    private int score = 60;
    private String name;
    private int age;

    @Override
    public String sayMyName() {
        return this.name;
    }

    @Override
    public String getName() {
        return "error" + this.name;
    }

    @Override
    public int getAge() {
        return 0;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
