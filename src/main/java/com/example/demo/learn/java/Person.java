package com.example.demo.learn.java;

/**
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2021/3/31
 */
public abstract class Person {

    public Person(String name){
        this.name = name;
    }
    public Person() {
    }

    public abstract String sayMyName();

    protected String name = "未知";
    private int birth = 2000;

    public void setBirth(int birth) {
        this.birth = birth;
    }

    public int getAge() {
        // 调用private方法
        return calcAge(2021);
    }

    /**
     * private方法:
     */
    private int calcAge(int currentYear) {
        return currentYear - this.birth;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
