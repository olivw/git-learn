package com.example.demo.learn.java;

import com.example.demo.learn.java.Student;

import java.util.ArrayList;
import java.util.List;

/**
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2021/3/31
 */
public class Learn {


    /**
     * @param args
     */
    public static void main(String[] args) {
        //创建Person实例
        Student hong = new Student();
        hong.setAge(21);

        Student ming = new Student();
        ming.setAge(22);

        System.out.println("姓名：" + ming.getName() + ";年龄：" + ming.getAge());

        Student student = new Student();

        System.out.println(student.getName() + ":" + student.getAge() + ":" + student.getScore());
        System.out.println("sayMyName:" + student.sayMyName());

        List list = new ArrayList();
        list.size();
        ArrayList l = new ArrayList();

    }
}
