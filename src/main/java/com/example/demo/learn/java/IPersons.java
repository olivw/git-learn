package com.example.demo.learn.java;

/**
 * update [序号][日期YYYY-MM-DD] [更改人姓名][变更描述]
 *
 * @author wangwenjing
 * @version v1.0
 * @date 2021/3/31
 */
public interface IPersons {
    String getName();
    int getAge();
    String sayMyName();
}
